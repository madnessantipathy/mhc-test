class View extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      display: [],
      suggestions: [],
      search: ""
    };
  }

  componentDidMount(){
    var componentThis = this
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
       var data = JSON.parse(this.responseText)
       var display = data.map((obj,index)=>{
         return <div className="currencycard" key={index}>
           <p>Name: {obj.name}</p>
           <p>Symbol: {obj.symbol}</p>
           <p>Circulating Supply: {obj.circulating_supply}</p>
           <p>Last Update: {obj.last_updated}</p>
           <p>Quoted Price: {obj.quote.USD.price}</p>
         </div>
       })
       var suggestions = data.map((obj,index)=>{
         return <option value = {obj.name} key={index}/>
       });
       componentThis.setState({
         display: display,
         data: data,
         suggestions: suggestions
       })
      };
    }
    xhttp.open("GET", "/getdata", true);
    xhttp.send();
  }

  searchData(e){
    var display = this.state.data.filter(obj=>obj.name.includes(e.target.value)).map((obj,index)=>{
      return <div className="currencycard" key={index}>
        <p>Name: {obj.name}</p>
        <p>Symbol: {obj.symbol}</p>
        <p>Circulating Supply: {obj.circulating_supply}</p>
        <p>Last Update: {obj.last_updated}</p>
        <p>Quoted Price(USD): {obj.quote.USD.price}</p>
      </div>
    })
    this.setState({
      display: display,
    })
  }

  render() {

    return (
      <div id="displayarea">
        <h1>Coin Market Cap Currency Listing</h1>
        <label>Search for crypto currency</label>
        <input type="text" list="currencies" onChange={this.searchData.bind(this)}/>
        <datalist id="currencies">
        {this.state.suggestions}
        </datalist>
        <div>
        {this.state.display}
        </div>
      </div>
    );
  }
}

ReactDOM.render(<View />, document.getElementById("view"));
