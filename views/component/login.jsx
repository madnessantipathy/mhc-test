var React = require("react");
var ReactDOM = require("react-dom");

class Login extends React.Component{
  render(){
    return(
      <div>
      <span class="error">{this.props.error}</span><br/>
        <form class="form" action="/login" method="POST">
          <div class="logincards">
            <label>Email</label>
            <input type="email" name="email"></input><br/>
          </div>
          <div class="logincards">
            <label>Password</label>
            <input type="password" name="password"></input><br/>
          </div>
          <button type="submit" value="submit">Login</button>
        </form>
      </div>
    )
  }
}

module.exports = Login;
