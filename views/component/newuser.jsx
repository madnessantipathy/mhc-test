var React = require("react");
var ReactDOM = require("react-dom");

class NewUser extends React.Component{
  render(){
    return(
      <div>
      <span class="error">{this.props.existing}</span>
        <form class="form" action="/newuser" method="POST">
          <div class="logincards">
            <label>Username</label>
            <input type="text" name="username"></input><br/>
          </div>
          <div class="logincards">
            <label>Email</label>
            <input type="email" name="email"></input><br/>
          </div>
          <div class="logincards">
            <label>Password</label>
            <input type="password" name="password"></input><br/>
          </div>
          <button type="submit" value="submit">Create New User</button>
        </form>
      </div>
    )
  }
}

module.exports = NewUser;
