var React = require("react");
var Layout = require("../component/layout.jsx")

class Home extends React.Component {
  render(){

    return (
      <Layout cookies={this.props.cookies}>
        <div>
          E-Mail: {this.props.data.email}
        </div>
        <div id="mainlogin">
          <div id="view"></div>
        </div>
        <script src="/jsx/view.jsx" type="text/babel"></script>
      </Layout>
    );
  }
}



module.exports = Home;
