var React = require("react");
var ReactDOM = require("react-dom");
var Layout = require("../component/layout.jsx")
import Login from '../component/login.jsx';
import NewUser from '../component/newuser.jsx';


class Home extends React.Component {
  render() {
    return (
      <Layout>
        <div id="mainlogin">
          <h1>Existing User</h1>
          <Login error={this.props.error}/>
          <h1>New User</h1>
          <NewUser existing={this.props.existing}/>
        </div>
      </Layout>
    );
  }
}



module.exports = Home;
