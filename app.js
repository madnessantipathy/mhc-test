const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')

app.use(bodyParser.json())
app.use(cookieParser())
app.use(express.static('public'));
app.use(express.urlencoded({
  extended: true
}));
const reactEngine = require('express-react-views').createEngine();

app.set('views', __dirname + '/views');
app.set('view engine', 'jsx');
app.engine('jsx', reactEngine);

const setRoutesFunction = require('./routes');
setRoutesFunction(app);

const PORT = process.env.PORT || 3000;
const server = app.listen(PORT, () => console.log('~~~ Tuning in to the waves of port '+PORT+' ~~~'));
let onClose = function(){
  server.close(() => {
    console.log('Process terminated')
  })
};

process.on('SIGTERM', onClose);
process.on('SIGINT', onClose);
