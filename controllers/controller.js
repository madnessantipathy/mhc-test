var MongoClient = require('mongodb').MongoClient;
const rp = require('request-promise');

module.exports = (db) => {
  let root = (request, response) => {
    response.render('index/root')
  };

  let login = (request, response) => {
    MongoClient.connect("mongodb://localhost:27017/mhcDB", function(err, db) {
      if(!err) {
        var dbo = db.db("mhcDB")
        var query = {email: request.body.email, password: request.body.password}
        dbo.collection("users").find(query).toArray(function(err, res) {
          if (!err){
            if (res.length > 0){
              var dataSet = {
                data: res[0],
                cookies: res[0].username,
              }
              response.cookie('id',res[0].username)
              response.cookie('email',res[0].email)
              response.cookie('login',"true")
              response.render('index/home', dataSet)

            }else{
              var dataSet = {
                error: "Incorrect email or password"
              }
              response.render('index/root', dataSet)
            }
          }
          db.close();
        });
      }
    });
  };

  let logout = (request, response) => {
    response.clearCookie('id', {path:'/'})
    response.clearCookie('email', {path:'/'})
    response.render('index/root')
  };

  let newuser = (request, response) => {
    MongoClient.connect("mongodb://localhost:27017/mhcDB", function(err, db) {
      if(!err) {
        var dbo = db.db("mhcDB")
        var query = {email: request.body.email}
        dbo.collection("users").find(query).toArray(function(err, res) {
          if (!err){
            if (res.length > 0){
              var dataSet = {
                existing: "Existing email in system"
              }
              response.render('index/root', dataSet)
            }else{
              dbo.collection("users").insertOne(request.body, function(err, res) {
                if (!err){
                  response.cookie('id',request.body.username)
                  response.cookie('email',request.body.email)
                  response.cookie('login',"true")
                  var dataSet = {
                    data: request.body,
                    cookies: request.body.username,
                  }
                  response.render('index/home', dataSet)
                }
              });
            }
          }
          db.close();
        });
      }
    });
  };

  let getdata = (request, response)=>{
    if(request.cookies.login === "true"){
      async function getData (){
        function callingOutsideFunction(){
          return new Promise(resolve =>{
            getCMCAPI(resolve)
          })
        }
        let currencyData = await callingOutsideFunction();
        response.send(currencyData);
      }
      getData()
    }else{
      response.send("You need to login first");
    }
  }

  function getCMCAPI (resolve){
    const requestOptions = {
      method: 'GET',
      uri: 'https://sandbox-api.coinmarketcap.com/v1/cryptocurrency/listings/latest',
      qs: {
        'start': '1',
        'limit': '5000',
        'convert': 'USD'
      },
      headers: {
        'X-CMC_PRO_API_KEY': 'aeaaf853-2fd7-4ab7-bad5-a3b08b4570e7'
      },
      json: true,
      gzip: true
    };
    rp(requestOptions).then(response => {
      resolve(response.data)
    }).catch((err) => {
      console.log('API call error:', err.message);
    });
  }

  return {
    root: root,
    login: login,
    logout: logout,
    newuser: newuser,
    getdata: getdata,
  };

}
