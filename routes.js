const path = require('path');

module.exports = (app, allModels) => {
  const objectControllerCallbacks = require('./controllers/controller')();
  app.get('/', objectControllerCallbacks.root);
  app.post('/login', objectControllerCallbacks.login);
  app.post('/logout', objectControllerCallbacks.logout);
  app.post('/newuser', objectControllerCallbacks.newuser);
  app.get('/getdata',objectControllerCallbacks.getdata)
};
