# mhc-test

Commentary of Project
This is a backend developer test provided by MHC Asia Group to assess candidates on suitability of the role as a backend developer.
This project was created with Node JS using Express JS for APIs. React Component was utilized for the frontend to provide users with interaction of the application with basic searching of currencies.

User Guide
Users will have to login with an existing account or create a new account. Users will have to take note that an error will display in the event of incorrect login credentials, or their selected e-mail has been taken during the account creation process.
Once logged in, users are brought to the homepage where a list of currencies with basic trading information is provided for them. Users have the option to search for a currency name. The display area will adjust accordingly and display the searched items. Users can also select from the dropbox suggestion list when they interact with the input field.

Installation Instructions
Requirement - NodeJS
User will clone the repository and navigate to the root directory of the project folder.
User will run "npm init" to install required dependencies for the application.
User will run "node/nodemon app.js" and open their browser to "127.0.0.1:3000" which will bring them to the login page.
